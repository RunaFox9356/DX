import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:loading_animation_widget/loading_animation_widget.dart';
import 'dart:convert';
import 'syndrome.dart';
import 'dialog.dart';
import 'option.dart';
import 'syndromedata.dart';
import 'mylist.dart';

class JsonIn extends StatefulWidget {
  final String title;
  final SyndromeData? syndromeData;

  JsonIn({Key? key, required this.title, this.syndromeData}) : super(key: key);

  @override
  _JsonIn createState() => _JsonIn();
}

class _JsonIn extends State<JsonIn> with WidgetsBindingObserver {
  List<Skill> _all_skills = [];
  List<Skill> _skills = [];
  List<Skill> _myskills = [];
  bool isLike = false;
  bool shouldFilter = false;
  late SyndromeData _syndromeData;

  @override
  void initState() {
    super.initState();
    _syndromeData = widget.syndromeData ?? SyndromeData();
    loadJsonAsset();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    print("state = $state"); //状態を詳しく出す
    switch (state) {
      case AppLifecycleState.inactive:
        print('非アクティブになったときの処理');
        break;
      case AppLifecycleState.paused:
        print('停止されたときの処理');
        break;
      case AppLifecycleState.resumed:
        print('再開されたときの処理');
        break;
      case AppLifecycleState.detached:
        print('破棄されたときの処理');
        break;
      default:
        break;
    }
  }

  Future<void> loadJsonAsset() async {
    try {
      print("Loading JSON data...");
      String loadData = await rootBundle
          .loadString('assets/full_data_all_cleaned_with_dash.json');
      final List<dynamic> jsonResponse = json.decode(loadData);
      print("JSON data decoded successfully.");
      setState(() {
        _all_skills =
            jsonResponse.map((entry) => Skill.fromJson(entry)).toList();
      });
    } catch (e) {
      print("Error loading JSON: $e");
      setState(() {
        _all_skills = [
          Skill(
            no: "―",
            name: "Error loading data",
            syndrome: "―",
            level: "―",
            timing: "―",
            skill: "―",
            difficulty: "―",
            target: "―",
            range: "―",
            erosion: "―",
            restriction: "―",
            effect: "―",
            flavorText: "―",
          ),
        ];
      });
    }
  }

  List<String> _checkedList(List<Map<String, dynamic>> map) {
    List<String> _pick_value_lits = [];
    for (final moviesTitle in map) {
      if (moviesTitle['checked']) {
        _pick_value_lits.add(moviesTitle['value']);
      }
    }
    return _pick_value_lits;
  }

  List<Skill> _filteredSkills() {
    List<Skill> _filteredSkills = [];
    List<String> _pick_value_lits = [];

    _pick_value_lits = _checkedList(_syndromeData.getSyndromes());
    _filteredSkills = _all_skills
        .where((skill) => _pick_value_lits.contains(skill.syndrome))
        .toList();

    _pick_value_lits = _checkedList(_syndromeData.getErosionRates());
    _filteredSkills = _filteredSkills
        .where((skill) => _pick_value_lits.contains(skill.erosion))
        .toList();

    _pick_value_lits = _checkedList(_syndromeData.getSkills());
    _filteredSkills = _filteredSkills
        .where((skill) => _pick_value_lits.contains(skill.skill))
        .toList();

    _pick_value_lits = _checkedList(_syndromeData.getDifficulties());
    _filteredSkills = _filteredSkills
        .where((skill) => _pick_value_lits.contains(skill.difficulty))
        .toList();

    _pick_value_lits = _checkedList(_syndromeData.getRanges());
    _filteredSkills = _filteredSkills
        .where((skill) => _pick_value_lits.contains(skill.range))
        .toList();

    _pick_value_lits = _checkedList(_syndromeData.getRestrictions());
    _filteredSkills = _filteredSkills
        .where((skill) => _pick_value_lits.contains(skill.restriction))
        .toList();

    return _filteredSkills;
  }

  List<DropdownMenuItem<String>> buildDropdownMenuItems(
      List<Map<String, dynamic>> items) {
    return items.map<DropdownMenuItem<String>>((Map<String, dynamic> item) {
      return DropdownMenuItem<String>(
        value: item['value'],
        child: Text(item['value']),
      );
    }).toList();
  }

  @override
  Widget build(BuildContext context) {
    if (shouldFilter) {
      return FutureBuilder(
        future: _wait(7),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            _skills = _filteredSkills();
             shouldFilter =false;
            return _loadingWidget(100);
          } else {
            return _buildContent();
          }
          
        },
      );
    } else {
      _skills = _filteredSkills();
      return _buildContent();
    }
  }

  Widget _buildContent() {
    return Scaffold(
      body: Column(
        children: [
          Expanded(
            child: GridView.builder(
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 10, // 列の数を指定
                mainAxisSpacing: 8.0, // アイテム間の垂直スペース
                crossAxisSpacing: 8.0, // アイテム間の水平スペース
              ),
              itemCount: _skills.length,
              itemBuilder: (context, index) {
                return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20), // 角を丸くする
                      ),
                      padding: EdgeInsets.all(16), // パディングを追加
                    ),
                    onPressed: () {
                      if (isLike) {
                        _myskills.add(_skills[index]);
                      } else {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => DIALOG(skills: _skills[index]),
                          ),
                        );
                      }
                    },
                    child: Text(
                      '${_skills[index].name}, ${_skills[index].syndrome}',
                      textAlign: TextAlign.center,
                    ),
                  ),
                );
              },
            ),
          ),
        ],
      ),
      floatingActionButton: Column(
        verticalDirection: VerticalDirection.up, // childrenの先頭が下に配置されます。
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          // 1つ目のFAB
          FloatingActionButton(
            heroTag: "search",
            child: Icon(Icons.settings),
            backgroundColor: Colors.blue[200],
            onPressed: () async {
              final bool? result = await Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => OPTION(syndromeData: _syndromeData),
                ),
              );

              if (result != null && result) {
                setState(() {
                  print("もどり");
                  shouldFilter = true;
                  _skills = _filteredSkills();
                });
              }
            },
          ),
          // 2つ目のFAB
          Container(
            margin: EdgeInsets.only(bottom: 16.0),
            child: FloatingActionButton(
              heroTag: "scan",
              child: Icon(Icons.close),
              backgroundColor: Colors.pink[200],
              onPressed: () async {
                //loadJsonAsset();
              },
            ),
          ),
          // 3つ目のFAB
          Container(
            margin: EdgeInsets.only(bottom: 16.0),
            child: FloatingActionButton(
              heroTag: "scan",
              child: Icon(Icons.favorite),
              backgroundColor: Color.fromARGB(255, 222, 105, 10),
              onPressed: () async {
                setState(() {
                  isLike = !isLike;
                });
              },
            ),
          ),
          Container(
            margin: EdgeInsets.only(bottom: 16.0),
            child: FloatingActionButton(
              heroTag: "scan",
              child: Icon(Icons.home),
              backgroundColor: Color.fromARGB(255, 127, 222, 10),
              onPressed: () async {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) =>
                        HomePage(key: Key('テスト'), skills: _myskills),
                  ),
                ).then(
                  (value) {
                    setState(() {
                      print("もどり");
                      //_skills = _filteredSkills();
                    });
                  },
                );
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _loadingWidget(double size) {
    return Center(
      child: LoadingAnimationWidget.twoRotatingArc(
        color: Colors.blue,
        size: size,
      ),
    );
  }

  Future _wait(seconds) async {
    return await Future.delayed(Duration(seconds: seconds));
  }
}
