class SyndromeData {
  final List<Map<String, dynamic>> syndromes = [
    {'value': 'ALL', 'checked': true},
    {'value': '―', 'checked': true},
    {'value': 'エグザイル', 'checked': true},
    {'value': 'ハヌマーン', 'checked': true},
    {'value': 'モルフェウス', 'checked': true},
    {'value': 'アザトース', 'checked': true},
    {'value': 'エンジェルハイロゥ', 'checked': true},
    {'value': 'ブラックドッグ', 'checked': true},
    {'value': 'ブラム＝ストーカー', 'checked': true},
    {'value': 'キュマイラ', 'checked': true},
    {'value': 'ソラリス', 'checked': true},
    {'value': 'サラマンダー', 'checked': true},
    {'value': '古代種', 'checked': true},
    {'value': '申し子', 'checked': true},
    {'value': '一般', 'checked': true},
  ];

  final List<Map<String, dynamic>> erosionRates = [
    {'value': 'ALL', 'checked': true},
    {'value': '―', 'checked': true},
    {'value': '0', 'checked': true},
    {'value': '1', 'checked': true},
    {'value': '2', 'checked': true},
    {'value': '3', 'checked': true},
    {'value': '4', 'checked': true},
    {'value': '5', 'checked': true},
    {'value': '6', 'checked': true},
    {'value': '4d10', 'checked': true},
    {'value': '効果参照', 'checked': true},
    {'value': '浸蝕率基本値を+2', 'checked': true},
    {'value': '浸蝕率基本値を+3', 'checked': true},
    {'value': '浸蝕率基本値を+4', 'checked': true},
    {'value': '侵蝕率を+3', 'checked': true},
    {'value': '侵蝕率を+5', 'checked': true},
    {'value': '侵蝕率を+10', 'checked': true},
  ];

  final List<Map<String, dynamic>> skills = [
    {'value': 'ALL', 'checked': true},
    {'value': '―', 'checked': true},
    {'value': 'RC', 'checked': true},
    {'value': '効果参照', 'checked': true},
    {'value': '白兵・射撃', 'checked': true},
    {'value': '交渉', 'checked': true},
    {'value': '意志', 'checked': true},
    {'value': '精神', 'checked': true},
    {'value': '調達', 'checked': true},
    {'value': '回避', 'checked': true},
    {'value': '情報：', 'checked': true},
  ];

  final List<Map<String, dynamic>> difficulties = [
    {'value': 'ALL', 'checked': true},
    {'value': '―', 'checked': true},
    {'value': '自動成功', 'checked': true},
    {'value': '対決', 'checked': true},
    {'value': '効果参照', 'checked': true},
  ];

  final List<Map<String, dynamic>> ranges = [
    {'value': 'ALL', 'checked': true},
    {'value': '―', 'checked': true},
    {'value': '視界', 'checked': true},
    {'value': '至近', 'checked': true},
    {'value': '武器', 'checked': true},
    {'value': '効果参照', 'checked': true},
    {'value': '範囲（選択）', 'checked': true},
  ];

  final List<Map<String, dynamic>> restrictions = [
    {'value': 'ALL', 'checked': true},
    {'value': '―', 'checked': true},
    {'value': '80%', 'checked': true},
    {'value': '100%', 'checked': true},
    {'value': '1シナリオに1回', 'checked': true},
    {'value': '1ラウンドに1回', 'checked': true},
    {'value': '1シーンに1回', 'checked': true},
  ];

  List<List<Map<String, dynamic>>> get() {
    List<List<Map<String, dynamic>>> Syndrome_Data_list = [];

    Syndrome_Data_list.add(syndromes);
    Syndrome_Data_list.add(erosionRates);
    Syndrome_Data_list.add(skills);
    Syndrome_Data_list.add(difficulties);
    Syndrome_Data_list.add(ranges);
    Syndrome_Data_list.add(restrictions);

    return Syndrome_Data_list;
  }

  List<Map<String, dynamic>> getSyndromes() => syndromes;
  List<Map<String, dynamic>> getErosionRates() => erosionRates;
  List<Map<String, dynamic>> getSkills() => skills;
  List<Map<String, dynamic>> getDifficulties() => difficulties;
  List<Map<String, dynamic>> getRanges() => ranges;
  List<Map<String, dynamic>> getRestrictions() => restrictions;

  setSyndromes(int index, String key, change_value) {
    syndromes[index].update(key, (value) => change_value);
  }

  setErosionRates(int index, String key, change_value) {
    erosionRates[index].update(key, (value) => change_value);
  }

  setSkills(int index, String key, change_value) {
    skills[index].update(key, (value) => change_value);
  }

  setDifficulties(int index, String key, change_value) {
    difficulties[index].update(key, (value) => change_value);
  }

  setRanges(int index, String key, change_value) {
    ranges[index].update(key, (value) => change_value);
  }

  setRestrictions(int index, String key, change_value) {
    restrictions[index].update(key, (value) => change_value);
  }
}
