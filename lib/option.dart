import 'package:flutter/material.dart';
import 'syndromedata.dart';

class OPTION extends StatefulWidget {
  final SyndromeData syndromeData;
  OPTION({required this.syndromeData});

  @override
  _OPTION createState() => _OPTION();
}

class _OPTION extends State<OPTION> {
  late SyndromeData _syndromeData;
  bool is_Change = false;
  @override
  void initState() {
    super.initState();
      is_Change = false;
    _syndromeData = widget.syndromeData; // コンストラクタで受け取ったスキルリストを初期化
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('option'),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context, is_Change); // bool値を渡す
          },
        ),
      ),
      body: ListView(
          shrinkWrap: true, // 内部リストのサイズを子要素に合わせる
          children: [
            _SyndromesContent(),
            _ErosionRatesContent(),
            _SkillsContent(),
            _DifficultiesContent(),
            _RangesContent(),
            _RestrictionsContent()
          ]),
    );
  }

  Widget _SyndromesContent() {
    return ExpansionTile(
      title: const Text('シンドローム'),
      children: [
        SizedBox(
          //画面外にオブジェクトを置くための物リストで置くものはこれで画面外に設置できる
          child: ListView.builder(
            physics: const NeverScrollableScrollPhysics(), // 内部スクロールを無効にする
            shrinkWrap: true, // 内部リストのサイズを子要素に合わせる
            itemCount: _syndromeData.getSyndromes().length,
            itemBuilder: (context, index) {
              //チェックボックス一つの処理
              return CheckboxListTile(
                title: Text(_syndromeData.getSyndromes()[index]['value']),
                subtitle: Text(_syndromeData.getSyndromes()[index]['checked']
                    ? "true"
                    : "false"),
                value: _syndromeData.getSyndromes()[index]['checked'],
                onChanged: (bool? checkedValue) {
                  // セットステータス付けないと値が変更されない
                  setState(() {
                    is_Change = true;
                    debugPrint('debug: $checkedValue');
                    _syndromeData.setSyndromes(index, 'checked', checkedValue!);
                  });
                },
              );
            },
          ),
        ),
      ],
    );
  }

  Widget _ErosionRatesContent() {
    return ExpansionTile(
      title: const Text('浸食率'),
      children: [
        SizedBox(
          //画面外にオブジェクトを置くための物リストで置くものはこれで画面外に設置できる
          child: ListView.builder(
            physics: const NeverScrollableScrollPhysics(), // 内部スクロールを無効にする
            shrinkWrap: true, // 内部リストのサイズを子要素に合わせる
            itemCount: _syndromeData.getErosionRates().length,
            itemBuilder: (context, index) {
              //チェックボックス一つの処理
              return CheckboxListTile(
                title: Text(_syndromeData.getErosionRates()[index]['value']),
                subtitle: Text(_syndromeData.getErosionRates()[index]['checked']
                    ? "true"
                    : "false"),
                value: _syndromeData.getErosionRates()[index]['checked'],
                onChanged: (bool? checkedValue) {
                  // セットステータス付けないと値が変更されない
                  setState(() {
                        is_Change = true;
                    debugPrint('debug: $checkedValue');
                    _syndromeData.setErosionRates(
                        index, 'checked', checkedValue!);
                  });
                },
              );
            },
          ),
        ),
      ],
    );
  }

  Widget _SkillsContent() {
    return ExpansionTile(
      title: const Text('使用技能'),
      children: [
        SizedBox(
          //画面外にオブジェクトを置くための物リストで置くものはこれで画面外に設置できる
          child: ListView.builder(
            physics: const NeverScrollableScrollPhysics(), // 内部スクロールを無効にする
            shrinkWrap: true, // 内部リストのサイズを子要素に合わせる
            itemCount: _syndromeData.getSkills().length,
            itemBuilder: (context, index) {
              //チェックボックス一つの処理
              return CheckboxListTile(
                title: Text(_syndromeData.getSkills()[index]['value']),
                subtitle: Text(_syndromeData.getSkills()[index]['checked']
                    ? "true"
                    : "false"),
                value: _syndromeData.getSkills()[index]['checked'],
                onChanged: (bool? checkedValue) {
                  // セットステータス付けないと値が変更されない
                  setState(() {
                        is_Change = true;
                    debugPrint('debug: $checkedValue');
                    _syndromeData.setSkills(index, 'checked', checkedValue!);
                  });
                },
              );
            },
          ),
        ),
      ],
    );
  }

  Widget _DifficultiesContent() {
    return ExpansionTile(
      title: const Text('判定'),
      children: [
        SizedBox(
          //画面外にオブジェクトを置くための物リストで置くものはこれで画面外に設置できる
          child: ListView.builder(
            physics: const NeverScrollableScrollPhysics(), // 内部スクロールを無効にする
            shrinkWrap: true, // 内部リストのサイズを子要素に合わせる
            itemCount: _syndromeData.getDifficulties().length,
            itemBuilder: (context, index) {
              //チェックボックス一つの処理
              return CheckboxListTile(
                title: Text(_syndromeData.getDifficulties()[index]['value']),
                subtitle: Text(_syndromeData.getDifficulties()[index]['checked']
                    ? "true"
                    : "false"),
                value: _syndromeData.getDifficulties()[index]['checked'],
                onChanged: (bool? checkedValue) {
                  // セットステータス付けないと値が変更されない
                  setState(() {
                        is_Change = true;
                    debugPrint('debug: $checkedValue');
                    _syndromeData.setDifficulties(
                        index, 'checked', checkedValue!);
                  });
                },
              );
            },
          ),
        ),
      ],
    );
  }

  Widget _RangesContent() {
    return ExpansionTile(
      title: const Text('射程'),
      children: [
        SizedBox(
          //画面外にオブジェクトを置くための物リストで置くものはこれで画面外に設置できる
          child: ListView.builder(
            physics: const NeverScrollableScrollPhysics(), // 内部スクロールを無効にする
            shrinkWrap: true, // 内部リストのサイズを子要素に合わせる
            itemCount: _syndromeData.getRanges().length,
            itemBuilder: (context, index) {
              //チェックボックス一つの処理
              return CheckboxListTile(
                title: Text(_syndromeData.getRanges()[index]['value']),
                subtitle: Text(_syndromeData.getRanges()[index]['checked']
                    ? "true"
                    : "false"),
                value: _syndromeData.getRanges()[index]['checked'],
                onChanged: (bool? checkedValue) {
                  // セットステータス付けないと値が変更されない
                  setState(() {
                        is_Change = true;
                    debugPrint('debug: $checkedValue');
                    _syndromeData.setRanges(index, 'checked', checkedValue!);
                  });
                },
              );
            },
          ),
        ),
      ],
    );
  }

  Widget _RestrictionsContent() {
    return ExpansionTile(
      title: const Text('制限'),
      children: [
        SizedBox(
          //画面外にオブジェクトを置くための物リストで置くものはこれで画面外に設置できる
          child: ListView.builder(
            physics: const NeverScrollableScrollPhysics(), // 内部スクロールを無効にする
            shrinkWrap: true, // 内部リストのサイズを子要素に合わせる
            itemCount: _syndromeData.getRestrictions().length,
            itemBuilder: (context, index) {
              //チェックボックス一つの処理
              return CheckboxListTile(
                title: Text(_syndromeData.getRestrictions()[index]['value']),
                subtitle: Text(_syndromeData.getRestrictions()[index]['checked']
                    ? "true"
                    : "false"),
                value: _syndromeData.getRestrictions()[index]['checked'],
                onChanged: (bool? checkedValue) {
                  // セットステータス付けないと値が変更されない
                  setState(() {
                        is_Change = true;
                    debugPrint('debug: $checkedValue');
                    _syndromeData.setRestrictions(
                        index, 'checked', checkedValue!);
                  });
                },
              );
            },
          ),
        ),
      ],
    );
  }
}
