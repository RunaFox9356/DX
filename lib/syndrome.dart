class Skill {
  final String no;
  final String name;
  final String syndrome;
  final String level;
  final String timing;
  final String skill;
  final String difficulty;
  final String target;
  final String range;
  final String erosion;
  final String restriction;
  final String effect;
  final String flavorText;

  Skill({
    required this.no,
    required this.name,
    required this.syndrome,
    required this.level,
    required this.timing,
    required this.skill,
    required this.difficulty,
    required this.target,
    required this.range,
    required this.erosion,
    required this.restriction,
    required this.effect,
    required this.flavorText,
  });

  factory Skill.fromJson(Map<String, dynamic> json) {
    return Skill(
      no: json['No'].toString(),
      name: json['名前']?.toString() ?? '―',
      syndrome: json['シンドローム']?.toString() ?? '―',
      level: json['Lv']?.toString() ?? '―',
      timing: json['タイミング']?.toString() ?? '―',
      skill: json['技能']?.toString() ?? '―',
      difficulty: json['難易度']?.toString() ?? '―',
      target: json['対象']?.toString() ?? '―',
      range: json['射程']?.toString() ?? '―',
      erosion: json['浸食率']?.toString() ?? '―',
      restriction: json['制限']?.toString() ?? '―',
      effect: json['効果']?.toString() ?? '―',
      flavorText: json['フレーバーテキスト']?.toString() ?? '―',
    );
  }

  int compareTolevel(Skill other) {
    return this.level.compareTo(other.level); // levelで比較
  }

  int compareToRange(Skill other) {
    return this.level.compareTo(other.range); // Rangeで比較
  }

  int sortName(Skill other) {
    return this.syndrome.compareTo(other.syndrome); // Nameで比較
  }
}
