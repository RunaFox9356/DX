import 'package:flutter/material.dart';
import 'syndrome.dart';
import 'json_in.dart';

class DIALOG extends StatefulWidget {
  final Skill skills;

  DIALOG({required this.skills});

  @override
  _DIALOG createState() => _DIALOG();
}

class _DIALOG extends State<DIALOG> {
  Skill _skills = Skill(
    no: "―",
    name: "Error loading data",
    syndrome: "―",
    level: "―",
    timing: "―",
    skill: "―",
    difficulty: "―",
    target: "―",
    range: "―",
    erosion: "―",
    restriction: "―",
    effect: "―",
    flavorText: "―",
  );

  @override
  void initState() {
    super.initState();
    _skills = widget.skills; // コンストラクタで受け取ったスキルリストを初期化
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('詳細'),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: Center(
        child: _buildDialogContent(),
      ),
    );
  }

  Widget _buildDialogContent() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          _skills.name,
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
        ),
        SizedBox(height: 20),
        Text(
          _skills.syndrome,
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 18),
        ),
        SizedBox(height: 20),
        Column(
          children: [
            TextButton(
              onPressed: () {
                // ①だった時の処理を記載
              },
              child: Text(
                _skills.level,
                style: TextStyle(
                  fontSize: 20,
                ),
              ),
            ),
            TextButton(
              onPressed: () {
                // ②だった時の処理を記載
              },
              child: Text(
                _skills.timing,
                style: TextStyle(
                  fontSize: 20,
                ),
              ),
            ),
            TextButton(
              onPressed: () {
                // ③だった時の処理を記載
              },
              child: Text(
                _skills.skill,
                style: TextStyle(
                  fontSize: 20,
                ),
              ),
            ),
            TextButton(
              onPressed: () {
                // ③だった時の処理を記載
              },
              child: Text(
                _skills.difficulty,
                style: TextStyle(
                  fontSize: 20,
                ),
              ),
            ),
            TextButton(
              onPressed: () {
                // ③だった時の処理を記載
              },
              child: Text(
                _skills.erosion,
                style: TextStyle(
                  fontSize: 20,
                ),
              ),
            ),
            TextButton(
              onPressed: () {
                // ③だった時の処理を記載
              },
              child: Text(
                _skills.target,
                style: TextStyle(
                  fontSize: 20,
                ),
              ),
            ),
            TextButton(
              onPressed: () {
                // ③だった時の処理を記載
              },
              child: Text(
                _skills.range,
                style: TextStyle(
                  fontSize: 20,
                ),
              ),
            ),
            TextButton(
              onPressed: () {
                // ③だった時の処理を記載
              },
              child: Text(
                _skills.restriction,
                style: TextStyle(
                  fontSize: 20,
                ),
              ),
            ),
            TextButton(
              onPressed: () {
                // ③だった時の処理を記載
              },
              child: Text(
                _skills.effect,
                style: TextStyle(
                  fontSize: 20,
                ),
              ),
            ),
            TextButton(
              onPressed: () {
                // ③だった時の処理を記載
              },
              child: Text(
                _skills.flavorText,
                style: TextStyle(
                  fontSize: 20,
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
