import 'package:flutter/material.dart';
import 'syndrome.dart';
import 'popskill.dart';

class HomePage extends StatefulWidget {
  final List<Skill> skills;
  const HomePage({Key? key, required this.skills}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  late List<Skill> _skills;
  @override
  void initState() {
    super.initState();
    _skills = widget.skills; // コンストラクタで受け取ったスキルリストを初期化
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Home'),
         leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context, false); // bool値を渡す
          },
        ),
      ),
      body: ListView.builder(
        itemCount: _skills.length,
        itemBuilder: (BuildContext context, int index) {
          return Dismissible(
            key: UniqueKey(),
            onDismissed: (direction) {
              setState(() {
                _skills.removeAt(index);
              });
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(content: Text('Skill dismissed')),
              );
            },
            background: Container(color: Colors.red),
            child: ExpansionTile(
              title: Text(_skills[index].name),
              children: [
                SkillTile(skill: _skills[index]),
              ],
            ),
          );
        },
      ),
    );
  }
}
