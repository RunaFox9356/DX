import 'package:flutter/material.dart';
import 'syndrome.dart';

class SkillTile extends StatelessWidget {
  final Skill skill;

  SkillTile({required this.skill});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('No: ${skill.no}'),
        Text('Name: ${skill.name}'),
        Text('Syndrome: ${skill.syndrome}'),
        Text('Level: ${skill.level}'),
        Text('Timing: ${skill.timing}'),
        Text('Skill: ${skill.skill}'),
        Text('Difficulty: ${skill.difficulty}'),
        Text('Target: ${skill.target}'),
        Text('Range: ${skill.range}'),
        Text('Erosion: ${skill.erosion}'),
        Text('Restriction: ${skill.restriction}'),
        Text('Effect: ${skill.effect}'),
        Text('Flavor Text: ${skill.flavorText}'),
      ],
    );
  }
}
